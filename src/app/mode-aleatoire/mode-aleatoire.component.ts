import { Component, OnInit } from '@angular/core';
import { Histoire } from '../models/histoire';
import { Reponse } from '../models/reponse';
import { HistoireService } from '../services/histoire.service';

@Component({
  selector: 'app-mode-aleatoire',
  templateUrl: './mode-aleatoire.component.html',
  styleUrls: ['./mode-aleatoire.component.css']
})
export class ModeAleatoireComponent implements OnInit {

  histoires: Histoire[] = [];
  reponses: Reponse[] = [];

  randomHistoire?: Histoire; // histoire selectionee aléatoirement
  histoiresNonRep: Histoire[] = []; // tableau des histoires sans réponses
  userReponse?: Reponse = {};

  constructor(private histoireService: HistoireService) { }

  ngOnInit(): void {
    this.histoires = this.getHistoires();
    this.reponses = this.getReponses();
    this.filterHistoiresNonRep();
  }

  random(): void { // fonction pour prendre une histoire au hasard parmi celles sans réponse
    this.randomHistoire = this.histoiresNonRep[Math.floor(Math.random() * this.histoiresNonRep.length)];
  }

  getHistoires(): Histoire[] {
    let tabHistoires: Histoire[]; // pour avoir un return
    this.histoireService.getHistoires()
    .subscribe(histoires  => tabHistoires = histoires);
    return tabHistoires;
  }

  getReponses(): Reponse[] {
    let tabReponses: Reponse[]; // pour avoir un return
    this.histoireService.getReponses()
    .subscribe(reponses  => tabReponses = reponses);
    return tabReponses;
  }

  submit(): void {
    console.log('Histoire: ', this.randomHistoire);
    console.log('Reponse: ', this.userReponse);

    this.histoireService.createResponse(this.randomHistoire, this.userReponse);
    this.filterHistoiresNonRep(); // on refiltre pour mettre a jour histoiresNonRep

    this.randomHistoire = null;
    this.userReponse = {};
  }

  filterHistoiresNonRep(): void  { // fonction appelée à l'init pour filtrer les histoires
    this.histoireService.filterHistoiresNonRep()
    .subscribe(histoiresNonRep => this.histoiresNonRep = histoiresNonRep);
  }

}
