import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModeAleatoireComponent } from './mode-aleatoire.component';

describe('ModeAleatoireComponent', () => {
  let component: ModeAleatoireComponent;
  let fixture: ComponentFixture<ModeAleatoireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModeAleatoireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModeAleatoireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
