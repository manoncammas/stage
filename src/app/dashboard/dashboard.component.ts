import { Component, OnInit, Input } from '@angular/core';
import { Histoire } from '../models/histoire';
import { Reponse } from '../models/reponse';
import { HistoireService } from '../services/histoire.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  histoires: Histoire[] = []; // tableau qui recup getHistoires
  reponses: Reponse[] = [];
  selectHistoire?: Histoire; // histoire selectionee pour afficher les details
  selectReponse?: Reponse; // reponse correspondant à l'histoire selectionee
  // pour les checkbox filtres :
  validated = false;
  answered = false;
  all = false;

  constructor(private histoireService: HistoireService) { }

  ngOnInit(): void {
    this.histoires = this.getHistoires();
    this.reponses = this.getReponses();
    // console.log('DASHBOARD ON INIT', this.histoires);
  }

  onSelect(histoire: Histoire): void { // enregistre l'histoire et la reponse selectionnees
    this.selectHistoire = histoire;
    this.selectReponse = this.reponses.find(reponse => reponse.idHistoire === this.selectHistoire.id);
    console.log(this.selectHistoire);
  }

  getHistoires(): Histoire[] {
    let tabHistoires: Histoire[]; // pour avoir un return
    this.histoireService.getHistoires()
    .subscribe(histoires  => tabHistoires = histoires);
    return tabHistoires;
  }

  getReponses(): Reponse[] {
    let tabReponses: Reponse[]; // pour avoir un return
    this.histoireService.getReponses()
    .subscribe(reponses  => tabReponses = reponses);
    return tabReponses;
  }

  filterDashboard(): void {
    let memHistoires: Histoire [] = this.getHistoires();
    if (this.all === false)
    {
      if (this.validated === true)
      {
        this.histoires = this.histoires.filter(histoire => histoire.validated === true);
        if (this.answered === true) {
          this.histoires = this.histoires.filter(histoire => histoire.idReponse);
        }
        else if (this.answered === false) {
          memHistoires = this.getHistoires();
          this.histoires = memHistoires;
          this.histoires = this.histoires.filter(histoire => histoire.validated === true);
        }
      }

      else if (this.validated === false) {
        memHistoires = this.getHistoires();
        this.histoires = memHistoires;
        if (this.answered === true) {
          this.histoires = this.histoires.filter(histoire => histoire.idReponse);
        }
      }
    }
    else {
      memHistoires = this.getHistoires();
      this.histoires = memHistoires;
    }
  }

  onChange(): void {
    this.filterDashboard();
    console.log('validated: ', this.validated);
    console.log('answered: ', this.answered);
    console.log('all: ', this.all);
  }

}
