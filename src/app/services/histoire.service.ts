import { Injectable } from '@angular/core';
import { Histoire } from '../models/histoire';
import { Reponse } from '../models/reponse';
import { HISTOIRES } from '../tab-histoires';
import { REPONSES } from '../tab-reponses';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HistoireService {

  private histoires: Histoire [];
  private reponses: Reponse [];

  constructor() {

    this.histoires = HISTOIRES;
    this.reponses = REPONSES;

   }

  filterHistoiresNonRep(): Observable<Histoire[]>  {
    const histoiresNonRep = of (this.histoires.filter(histoire => !histoire.idReponse));
    return histoiresNonRep;
  }

  getHistoires(): Observable<Histoire[]> {
    return of(this.histoires);
  }

  getReponses(): Observable<Reponse[]> {
    return of(this.reponses);
  }

  getHistoireByID(id: number): Observable<Histoire> {
    const histoire = this.histoires.find(h => h.id === id) as Histoire;
    return of(histoire);
  }

  updateHistoire(newHistoire: Histoire): void {
    const histoireToUpdate = this.histoires.find(h => h.id === newHistoire.id);
    const index = this.histoires.indexOf(histoireToUpdate);
    this.histoires.splice(index, 1, newHistoire); // remplace 1 element a l'index correspondant
  }

  createResponse(histoire: Histoire, reponse: Reponse): void {
    reponse.id = this.reponses[this.reponses.length - 1].id + 1;
    reponse.idHistoire = histoire.id;
    histoire.idReponse = reponse.id;
    this.reponses.push(reponse);
  }


}
