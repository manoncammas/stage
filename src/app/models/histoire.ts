import { Reponse } from './reponse';

export interface Histoire {
  id: number;
  title: string;
  content: string;
  creationDate: string;
  validated: boolean;
  idReponse?: number;
}
