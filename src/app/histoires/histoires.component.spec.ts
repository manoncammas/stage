import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoiresComponent } from './histoires.component';

describe('HistoiresComponent', () => {
  let component: HistoiresComponent;
  let fixture: ComponentFixture<HistoiresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoiresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoiresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
