import { Component, OnInit } from '@angular/core';
import { Histoire } from '../models/histoire';
import { Reponse } from '../models/reponse';
import { HISTOIRES } from '../tab-histoires';
import { HistoireService } from '../services/histoire.service';

@Component({
  selector: 'app-histoires',
  templateUrl: './histoires.component.html',
  styleUrls: ['./histoires.component.css']
})
export class HistoiresComponent implements OnInit {

  histoires: Histoire[] = []; // tableau total des histoires recup avec getHistoires
  reponses: Reponse[] = [];

  histoiresNonRep: Histoire[] = []; // tableau des histoires sans réponses
  selectHistoire?: Histoire; // histoire selectionée pour entrer une réponse
  userReponse?: Reponse = {};

  constructor(private histoireService: HistoireService) { }

  ngOnInit(): void { // a l'init on recup les histoires et on filtre
    this.histoires = this.getHistoires();
    this.reponses = this.getReponses();
    this.filterHistoiresNonRep();
  }

  onSelect(histoire: Histoire): void { // enregistre l'histoire selectionnee
    this.selectHistoire = histoire;
  }

  filterHistoiresNonRep(): void  {
    this.histoireService.filterHistoiresNonRep()
    .subscribe(histoiresNonRep => this.histoiresNonRep = histoiresNonRep);
  }

  submit(): void {
    console.log('Histoire: ', this.selectHistoire);
    console.log('Reponse: ', this.userReponse);

    this.histoireService.createResponse(this.selectHistoire, this.userReponse);
    this.filterHistoiresNonRep(); // on refiltre pour mettre a jour histoiresNonRep

    this.selectHistoire = null;
    this.userReponse = {};
  }

  getHistoires(): Histoire[] {
    let tabHistoires: Histoire[]; // pour avoir un return
    this.histoireService.getHistoires()
    .subscribe(histoires  => tabHistoires = histoires);
    return tabHistoires;
  }

  getReponses(): Reponse[] {
    let tabReponses: Reponse[]; // pour avoir un return
    this.histoireService.getReponses()
    .subscribe(reponses  => tabReponses = reponses);
    return tabReponses;
  }

}
