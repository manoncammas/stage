import { Component, OnInit, Input } from '@angular/core';
import { Histoire } from '../models/histoire';
import { Reponse } from '../models/reponse';
import { HistoireService } from '../services/histoire.service';

@Component({
  selector: 'app-histoire-detail',
  templateUrl: './histoire-detail.component.html',
  styleUrls: ['./histoire-detail.component.css']
})
export class HistoireDetailComponent implements OnInit {

  @Input() histoire?: Histoire;
  @Input() reponse?: Reponse;

  constructor(private histoireService: HistoireService) { }

  ngOnInit(): void {

  }

  validate(): void {
    this.histoire.validated = !this.histoire.validated;
    console.log(this.histoire);
    this.histoireService.updateHistoire(this.histoire);
  }
}
