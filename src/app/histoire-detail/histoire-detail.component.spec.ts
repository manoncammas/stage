import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoireDetailComponent } from './histoire-detail.component';

describe('HistoireDetailComponent', () => {
  let component: HistoireDetailComponent;
  let fixture: ComponentFixture<HistoireDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoireDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoireDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
