import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HistoiresComponent } from './histoires/histoires.component';
import { ModeAleatoireComponent } from './mode-aleatoire/mode-aleatoire.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'histoires', component: HistoiresComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'random', component: ModeAleatoireComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
