import { Reponse } from './models/reponse';

export const REPONSES: Reponse[] = [
  {
    id: 1,
    content: 'Ceci est la reponse a l\'histoire 1',
    idHistoire: 1,
  },
  {
    id: 2,
    content: 'Ceci est la reponse a l\'histoire 2',
    idHistoire: 2,
  },
  {
    id: 3,
    content: 'Ceci est la reponse a l\'histoire 3',
    idHistoire: 3,
  },
  {
    id: 4,
    content: 'Ceci est la reponse a l\'histoire 1',
    idHistoire: 4,
  },
  {
    id: 5,
    content: 'Ceci est la reponse a l\'histoire 5',
    idHistoire: 5,
  },
  {
    id: 6 ,
    content: 'Ceci est la reponse a l\'histoire 6',
    idHistoire: 6,
  }
];
