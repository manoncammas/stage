import { Histoire } from './models/histoire';

export const HISTOIRES: Histoire[] = [
  {
    id: 1,
    title: 'Titre histoire 1',
    content: 'Ceci est l\'histoire numero 1',
    creationDate: '01/01/01',
    validated: false,
    idReponse: 1
  },
  {
    id: 2,
    title: 'Titre histoire 2',
    content: 'Ceci est l\'histoire numero 2',
    creationDate: '02/02/02',
    validated: false,
    idReponse: 2
  },
  {
    id: 3,
    title: 'Titre histoire 3',
    content: 'Ceci est l\'histoire numero 3',
    creationDate: '03/03/03',
    validated: false,
    idReponse: 3
  },
  {
    id: 4,
    title: 'Titre histoire 4',
    content: 'Ceci est l\'histoire numero 4',
    creationDate: '04/04/04',
    validated: false,
    idReponse: 4
  },
  {
    id: 5,
    title: 'Titre histoire 5',
    content: 'Ceci est l\'histoire numero 5',
    creationDate: '05/05/05',
    validated: false,
    idReponse: 5
  },
  {
    id: 6,
    title: 'Titre histoire 6',
    content: 'Ceci est l\'histoire numero 6',
    creationDate: '06/06/06',
    validated: false,
    idReponse: 6
  },
  {
    id: 7,
    title: 'Titre histoire 7',
    content: 'Ceci est l\'histoire numero 7',
    creationDate: '07/07/07',
    validated: false
  },
  {
    id: 8,
    title: 'Titre histoire 8',
    content: 'Ceci est l\'histoire numero 8',
    creationDate: '08/08/08',
    validated: false
  },
  {
    id: 9,
    title: 'Titre histoire 9',
    content: 'Ceci est l\'histoire numero 9',
    creationDate: '09/09/09',
    validated: false
  },
];
