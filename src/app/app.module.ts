import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { HistoiresComponent } from './histoires/histoires.component';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HistoireDetailComponent } from './histoire-detail/histoire-detail.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ModeAleatoireComponent } from './mode-aleatoire/mode-aleatoire.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    HistoiresComponent,
    DashboardComponent,
    HistoireDetailComponent,
    NavbarComponent,
    ModeAleatoireComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
